/*
 * %W% %E% EurekaServer - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.entity.Help;
import com.example.service.HelpService;

/**
 * Class description goes here.
 * 
 * @since		10:15:09 AM
 * @author		nvuon
 */
@RestController
public class HelpController {
     
	@Autowired
	private HelpService helpService;
	
	@GetMapping("/help")
	public ResponseEntity<List<Help>> getAllHelp(){
		return ResponseEntity.ok().body(helpService.getAllHelp());
	}
	
	@PostMapping("/help/{id}")
	public ResponseEntity<Help> getHelpById(@PathVariable long id){
		return ResponseEntity.ok().body(helpService.getHelpById(id));
	}
	
	@PostMapping("/help")
	public ResponseEntity<Help> createHelp(@RequestBody Help help){
		return ResponseEntity.ok().body(this.helpService.createHelp(help));
	}
	
	@PutMapping("/help/{id")
	public ResponseEntity<Help> updateHelp(@PathVariable long id, @RequestBody Help help){
		help.setHelp_id(id);
		return ResponseEntity.ok().body(this.helpService.updateHelp(help));
	}
	
	@PutMapping("/help/{id}")
	public ResponseEntity<Help> deleteHelp(@PathVariable long id, @RequestBody Help help){
		help.setHelp_id(id);
		return ResponseEntity.ok().body(this.helpService.updateHelp(help));
	}
	
}
