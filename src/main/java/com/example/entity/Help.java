/*
 * %W% %E% EurekaServer - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class description goes here.
 * 
 * @since		5:21:56 PM
 * @author		nvuon
 */

@Entity
@Table(name = "HELP")
public class Help {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "help_id")
	private long help_id;
	
	@Column(name = "help_name")
	private String help_name;
	
	@Column(name = "parent_help_id")
	private Integer parent_help_id;
	
	@Column(name = "type")
	private String type;
	
	@Column(name = "position")
	private String position;
	
	@Column(name = "content")
	private String content;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "created_user")
	private String created_user;

	/**
	 * @return the help_id
	 */
	public long getHelp_id() {
		return help_id;
	}

	/**
	 * @param help_id the help_id to set
	 */
	public void setHelp_id(long help_id) {
		this.help_id = help_id;
	}

	/**
	 * @return the help_name
	 */
	public String getHelp_name() {
		return help_name;
	}

	/**
	 * @param help_name the help_name to set
	 */
	public void setHelp_name(String help_name) {
		this.help_name = help_name;
	}

	/**
	 * @return the parent_help_id
	 */
	public Integer getParent_help_id() {
		return parent_help_id;
	}

	/**
	 * @param parent_help_id the parent_help_id to set
	 */
	public void setParent_help_id(Integer parent_help_id) {
		this.parent_help_id = parent_help_id;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the position
	 */
	public String getPosition() {
		return position;
	}

	/**
	 * @param position the position to set
	 */
	public void setPosition(String position) {
		this.position = position;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the created_user
	 */
	public String getCreated_user() {
		return created_user;
	}

	/**
	 * @param created_user the created_user to set
	 */
	public void setCreated_user(String created_user) {
		this.created_user = created_user;
	}

	/**
	 * @param help_id
	 * @param help_name
	 * @param parent_help_id
	 * @param type
	 * @param position
	 * @param content
	 * @param status
	 * @param created_user
	 */
	public Help(long help_id, String help_name, Integer parent_help_id, String type, String position, String content,
			String status, String created_user) {
		super();
		this.help_id = help_id;
		this.help_name = help_name;
		this.parent_help_id = parent_help_id;
		this.type = type;
		this.position = position;
		this.content = content;
		this.status = status;
		this.created_user = created_user;
	}

	/**
	 * @return
	 */

	
	
}
