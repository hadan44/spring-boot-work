/*
 * %W% %E% EurekaServer - nvuon
 *
 * Copyright (c) 2017-2018 SMAC VNIT, jsc. All Rights Reserved. 
 *
 * This software is the confidential and proprietary information of SMAC VNIT, jsc. 
 * ("Confidential Information"). You shall not disclose such Confidential Information 
 * and shall use it only in accordance with the terms of the license agreement you entered 
 * into with SMAC.
 *
 * SMAC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF 
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED 
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. SMAC SHALL NOT BE LIABLE FOR 
 * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR 
 * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
*/
package com.example.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.entity.Help;
import com.example.exception.ResourceNotFoundException;
import com.example.repository.HelpRepository;

/**
 * Class description goes here.
 * 
 * @since		5:16:09 AM
 * @author		nvuon
 */
public class HelpServiceimp implements HelpService{
    @Autowired
    private HelpRepository helpRepository;
    
	@Override
	public Help createHelp(Help help) {
		return helpRepository.save(help);
	}

	@Override
	public List<Help> getAllHelp() {
		return this.helpRepository.findAll();
	}

	@Override
	public void deleteHelp(long id) {
		Optional<Help> helpDb = this.helpRepository.findById(id);
		if(helpDb.isPresent()) {
			Help helpUpdate = helpDb.get();
			helpUpdate.setStatus("0");
		} else {
			throw new ResourceNotFoundException("Record now found with id" + id);
		}
	}

	@Override
	public Help getHelpById(long id) {
		Optional<Help> helpDb = this.helpRepository.findById(id);
		if(helpDb.isPresent()) {
			return helpDb.get();
		} else {
			throw new ResourceNotFoundException("Record now found with id" + id);
		}
	}

	@Override
	public Help updateHelp(Help help) {
        Optional<Help> helpDb = this.helpRepository.findById(help.getHelp_id());
		
		if(helpDb.isPresent()) {
			Help helpUpdate = helpDb.get();
			helpUpdate.setHelp_id(help.getHelp_id());
			helpUpdate.setHelp_name(help.getContent());
			helpUpdate.setParent_help_id(help.getParent_help_id());
			helpUpdate.setType(help.getType());
			helpUpdate.setPosition(help.getPosition());
			helpUpdate.setContent(help.getContent());
			helpUpdate.setStatus(help.getStatus());
			helpUpdate.setCreated_user(help.getCreated_user());
			return helpUpdate;
		} else {
			throw new ResourceNotFoundException("Record now found with id" + help.getHelp_id());
		}
	}
}
